﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace NumberParserLib
{
    public class LogicalFileLine
    {
        public char[,] Line { get { return _line; } }

        private char[,] _line;
        //Assumption: all text file lines will be 4lines 
        public LogicalFileLine(string textLine1, string textLine2, string textLine3, string textLine4)
        {
            
            List<string> lines = new List<string>()
                { String.IsNullOrEmpty( textLine1 )? "" : textLine1,
                 String.IsNullOrEmpty( textLine2 )? "" : textLine2,
                 String.IsNullOrEmpty( textLine3 )? "" : textLine3,
                 String.IsNullOrEmpty( textLine4 )? "" : textLine4,
            };

            var maxLength = lines.ToArray().OrderByDescending(s => s.Length).First().Length;
            _line = new char[4, maxLength];

            int row = 0;
            foreach (var item in lines)
            {
                //convert all '\0' to spaces
                var newitem = item.PadRight(maxLength);
                var chars = newitem.ToCharArray();
                for (int i = 0; i < chars.Length; i++)
                {

                    _line[row, i] = chars[i];
                }
                row++;
            }

            
        }
    }
}
