﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NumberParserLib
{
    public class Pattern
    {
        public char[,] CharPattern { get; set; }

        public Pattern(char[,] pattern)
        {
            CharPattern = pattern;
        }

        //Not a true hash;
        //hash is used to compare the patterns. 
        //for this we just flatten out the array into a long comparable string
        public string PatternHash
        {
            get
            {
                StringBuilder b = new StringBuilder();
                foreach (var item in CharPattern)
                {
                    b.Append(item.ToString()).Replace('\t',' ');
                }
                return b.ToString();
            }
        }
    }
}
