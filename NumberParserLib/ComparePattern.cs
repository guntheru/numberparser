﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NumberParserLib
{
    internal class ComparePattern : Pattern
    {
        internal string PatternValue { get; set; }        

        internal ComparePattern(string patternValue, char[,] pattern): base(pattern)
        {
            PatternValue = patternValue;
        }
    }
}
