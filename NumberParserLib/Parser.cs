﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NumberParserLib
{
    public class Parser
    {
        List<ComparePattern> patternList;


        public Parser() => CreateAvailablePatterns();

        private void CreateAvailablePatterns()
        {
            patternList = new List<ComparePattern>();

            patternList.Add( new ComparePattern("1", new char[4, 1]
            {
                { '|'},
                { '|'},
                { '|'},
                { '|'}
            }
            ));

            patternList.Add(new ComparePattern("2", new char[4, 3]
            {
                { '-','-','-'},
                { ' ', '_', '|'},
                { '|', '	', ' '},
                { '-','-','-'}
            }
            ));

            patternList.Add(new ComparePattern("3", new char[4, 3]
            {
                { '-','-','-'},
                { ' ', '/', ' '},
                { ' ', '\\', ' '},
                { '-','-',' '}
            }
            ));

            patternList.Add(new ComparePattern("4", new char[4, 5]
            {
                { '|',' ',' ',' ','|'},
                { '|','_','_','_','|'},
                { ' ',' ',' ',' ','|'},
                { ' ',' ',' ',' ','|'},                
            }
            ));



            patternList.Add(new ComparePattern("5", new char[4, 5]
            {
                { '-','-','-','-','-'},
                { '|','_','_','_', ' '},
                { ' ',' ',' ',' ','|'},
                { '_','_','_','_','|'},
            }
            ));

            patternList.Add(new ComparePattern("6", new char[4, 5]
            {
                { '-','-','-','-','-'},
                { '|','_','_','_','_'},
                { '|',' ',' ',' ','|'},
                { '|','_','_','_','|'},
            }
            ));

            patternList.Add(new ComparePattern("8", new char[4, 5]
            {
                { '|','-','-','-','|'},
                { '|','_','_','_','|'},
                { '|',' ',' ',' ','|'},
                { '|','_','_','_','|'},
            }
            ));



        }

        public string ParsePattern(Pattern pattern)
        {
            
            int height = pattern.CharPattern.GetLength(0);
            int width = pattern.CharPattern.GetLength(1);
            //find only where the shape of the patterns are equal
            var availablePatterns = patternList.Where(p => p.CharPattern.GetLength(0) == height && p.CharPattern.GetLength(1) == width);
            if (availablePatterns.Count() == 0)
            {
                return String.Empty;
            }
            else
            {
                //compare hashes. that should be it
                foreach (var currentPattern in availablePatterns)
                {
                    if (pattern.PatternHash == currentPattern.PatternHash)
                    {
                        return currentPattern.PatternValue;
                    }
                }
                return String.Empty;

            }
        }

        public string ParseLine(char[,] fullLine)
        {
            //basically the mehtod will search at each position all possible patterns available
            //if one is found it is added to the result string and then the position is moved by the length of the found pattern
            //if none found then move by 1;

            var result = new StringBuilder(); 

            var lineLength = fullLine.GetLength(1);
            int currentXPos = 0;
            for (int i = 0; i < lineLength; i++)
            {
                //at each position try and find the pattern in the collection of patters available
                //if found the add to overal result and move to the next position
                var found = false;
                bool addSpaceCol = false;
                var patternWidth = 0; 
                var patternHeight = 0;


                foreach (var p in patternList)
                {

                    patternWidth = p.CharPattern.GetLength(1);
                    patternHeight = p.CharPattern.GetLength(0);
                    if (currentXPos + patternWidth == lineLength) //we are extactly reaching to the end of the line//no need to add a space column
                    {
                        addSpaceCol = false;
                    }
                    else if (currentXPos + patternWidth > lineLength) // the current test pattern will reach above the line limit// no need to carry on testing
                    {
                        break;
                    }
                    else // we have space to add a space colum
                    {
                        addSpaceCol = true;
                    }

                    //extract the current shape of the test pattern from the full line
                    char[,] currExtractedPattern = extractComparePattern(fullLine, currentXPos, addSpaceCol, patternWidth, patternHeight);

                    char[,] currComparePattern = null;
                    if (addSpaceCol)
                    {
                        var testPWihtSpace = ResizeMultiArray<char>(p.CharPattern, patternHeight, patternWidth + 1);
                        AppendSpaceColumnToPattern(patternWidth + 1, patternHeight, testPWihtSpace);
                        currComparePattern = testPWihtSpace;
                    }
                    else
                    {
                        currComparePattern = p.CharPattern;
                    }


                    var compareHash = new Pattern(currExtractedPattern).PatternHash;
                    var testHash = new Pattern(currComparePattern).PatternHash;
                   
                    if (compareHash == testHash)
                    {
                        //the current pattern is found the the full line
                        result.AppendFormat("{0} ", p.PatternValue);
                        found = true;
                        break;//no need to carry on searching
                    }
                    else
                    {
                        found = false;
                    }

                }


                if (found)
                {
                    currentXPos += patternWidth;
                }
                else
                {
                    currentXPos += 1;
                }
            }
            return result.ToString().TrimEnd();
        }

        private char[,] extractComparePattern(char[,] fullLine, int currentXPos, bool appendColumn, int patternWidth, int patternHeight)
        {
            char[,] currComparePattern = appendColumn ?
                                    new char[patternHeight, patternWidth + 1]
                                    : new char[patternHeight, patternWidth];

            patternWidth = appendColumn ?
                patternWidth + 1
                : patternWidth;

            for (int k = 0; k < patternWidth; k++)
            {
                for (int j = 0; j < patternHeight; j++)
                {
                    currComparePattern[j, k] = fullLine[j, currentXPos + k];
                }
            }

            return currComparePattern;
        }

        private  void AppendSpaceColumnToPattern(int patternWidth, int patternHeight, char[,] copy)
        {
            for (int j = 0; j < patternHeight; j++)
            {
                //patternWidth - 1 is the last column in the array
                copy[j, patternWidth - 1] = ' ';
            }
        }

        private T[,] ResizeMultiArray<T>(T[,] original, int rows, int cols)
        {
            var copyArray = new T[rows, cols];
            int minRows = Math.Min(rows, original.GetLength(0));
            int minCols = Math.Min(cols, original.GetLength(1));
            for (int i = 0; i < minRows; i++)
            {
                for (int j = 0; j < minCols; j++)
                {
                    copyArray[i, j] = original[i, j];
                }
            }
            return copyArray;
        }
    }
}
