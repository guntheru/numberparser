﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace NumberParserLib
{
    public class LineReader
    {
        public List<LogicalFileLine> GroupedLines { get; internal set; }
        public  LineReader(string[] lines)
        {
            GroupedLines = new List<LogicalFileLine>();
            foreach (var item in lines.Batch(4))
            {
                var logicalLine = new LogicalFileLine(
                    item.ElementAtOrDefault(0),
                    item.ElementAtOrDefault(1),
                    item.ElementAtOrDefault(2),
                    item.ElementAtOrDefault(3));
                GroupedLines.Add(logicalLine);
            }
        }
    }
}
