﻿using NumberParserLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParserApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(@"Numbers.txt");

            var lineReader = new LineReader(lines);
            var parser = new Parser();

            foreach (var item in lineReader.GroupedLines)
            {
                var text = parser.ParseLine(item.Line);
                Console.WriteLine(text);
                    
            }
            Console.ReadLine();

        }
    }
}
