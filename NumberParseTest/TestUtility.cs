﻿using NumberParserLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParseTest
{
    class TestUtility
    {
        public static Pattern Get_1_Pattern()
        {
            return  new Pattern(new char[4, 1]
           {
                { '|'},
                { '|'},
                { '|'},
                { '|'}
           }
           );
        }

        public static Pattern Get_2_Pattern()
        {
            return  new Pattern(new char[4, 3]
            {
                { '-','-','-'},
                { ' ', '_', '|'},
                { '|', ' ', ' '},
                { '-','-','-'}
            }
            );
        }

        public static Pattern Get_3_Pattern()
        {
            return new Pattern(new char[4, 3]
            {
                { '-','-','-'},
                { ' ', '/', ' '},
                { ' ', '\\', ' '},
                { '-','-',' '}
            }
            );
        }

        public static Pattern Get_4_Pattern()
        {
            return new Pattern(new char[4, 5]
           {
                { '|',' ',' ',' ','|'},
                { '|','_','_','_','|'},
                { ' ',' ',' ',' ','|'},
                { ' ',' ',' ',' ','|'},
           }
           );
        }

        public static Pattern Get_5_Pattern()
        {
            return  new Pattern(new char[4, 5]
            {
                { '-','-','-','-','-'},
                { '|','_','_','_',' '},
                { ' ',' ',' ',' ','|'},
                { '_','_','_','_','|'},
            }
            );
        }

        public static Pattern Get_6_Pattern()
        {
            return new Pattern(new char[4, 5]
            {
                    { '-','-','-','-','-'},
                    { '|','_','_','_','_'},
                    { '|',' ',' ',' ','|'},
                    { '|','_','_','_','|'},
            }
            );
        }

        public static Pattern Get_8_Pattern()
        {
            return new Pattern(new char[4, 5]
            {
                    { '|','-','-','-','|'},
                    { '|','_','_','_','|'},
                    { '|',' ',' ',' ','|'},
                    { '|','_','_','_','|'},
            }
            );
        }

        public static Pattern Get_Space_Pattern()
        {
            return new Pattern(new char[4, 1]
            {
                    { ' ' },
                    { ' ' },
                    { ' ' },
                    { ' ' },
            }
            );
        }

        internal static char[,] CreateFullLineForTest_1_8__1()
        {
            var line1 = @"| |---|  |".ToCharArray();
            var line2 = @"| |___|  |".ToCharArray();
            var line3 = @"| |   |  |".ToCharArray();
            var line4 = @"| |___|  |".ToCharArray();

            char[,] textLines = new char[4, line1.Length];

            for (int i = 0; i < line1.Length; i++)
            {
                textLines[0, i] = line1[i];
                textLines[1, i] = line2[i];
                textLines[2, i] = line3[i];
                textLines[3, i] = line4[i];

            }

            return textLines; ;
        }

        internal static char[,] CreateFullLineForTest_3_2__3_1_1_1()
        {
            
            

            var line1 = @"--- ---  --- | | |".ToCharArray();
            var line2 = @" /   _|   /  | | |".ToCharArray();
            var line3 = @" \  |     \  | | |".ToCharArray();
            var line4 = @"--  ---  --  | | |".ToCharArray();

            char[,] textLines = new char[4, line1.Length];

            for (int i = 0; i < line1.Length; i++)
            {
                textLines[0, i] = line1[i];
                textLines[1, i] = line2[i];
                textLines[2, i] = line3[i];
                textLines[3, i] = line4[i];

            }

            return textLines; ;
        }

        internal static char[,] CreateFullLineForTest_1_2_3_4_5_6()
        {
            
            char[,] textLines = new char[4,27];

            var line1 = @"| --- --- |   | ----- -----".ToCharArray();
            var line2 = @"|  _|  /  |___| |___  |____".ToCharArray();
            var line3 = @"| |    \      |     | |   |".ToCharArray();
            var line4 = @"| --- --      | ____| |___|".ToCharArray();

            for (int i = 0; i < 27; i++)
            {
                textLines[0, i] = line1[i];
                textLines[1, i] = line2[i];
                textLines[2, i] = line3[i];
                textLines[3, i] = line4[i];

            }

            return textLines; ;
        }

        
    }
}
