﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberParserLib;

namespace NumberParseTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Find_Provided_Pattern_1()
        {          

            var parser = new Parser();
            string value = parser.ParsePattern(TestUtility.Get_1_Pattern());
            Assert.AreEqual("1", value);
        }

        [TestMethod]
        public void Find_Provided_Pattern_2()
        {           

            var parser = new Parser();
            string value = parser.ParsePattern(TestUtility.Get_2_Pattern());
            Assert.AreEqual("2", value);
        }

        [TestMethod]
        public void Find_Provided_Pattern_3()
        {           

            var parser = new Parser();
            string value = parser.ParsePattern(TestUtility.Get_3_Pattern());
            Assert.AreEqual("3", value);
        }


        [TestMethod]
        public void Find_Provided_Pattern_4()
        {         

            var parser = new Parser();
            string value = parser.ParsePattern(TestUtility.Get_4_Pattern());
            Assert.AreEqual("4", value);
        }

        [TestMethod]
        public void Find_Provided_Pattern_5()
        {
            
            var parser = new Parser();
            string value = parser.ParsePattern(TestUtility.Get_5_Pattern());
            Assert.AreEqual("5", value);
        }

        [TestMethod]
        public void Do_Not_Find_Provided_Pattern_6()
        {

            var parser = new Parser();
            string value = parser.ParsePattern(TestUtility.Get_6_Pattern());
            Assert.AreEqual("6", value);
        }

        [TestMethod]
        public void Parse_Full_Line_1_2_3_4_5_6()
        {
            var fullLine = TestUtility.CreateFullLineForTest_1_2_3_4_5_6();
            var parser = new Parser();
            string output = parser.ParseLine(fullLine);
            Assert.AreEqual("1 2 3 4 5 6", output);


        }

        [TestMethod]
        public void Parse_Full_Line_3_2__3_1_1_1()
        {
            var fullLine = TestUtility.CreateFullLineForTest_3_2__3_1_1_1();
            var parser = new Parser();
            string output = parser.ParseLine(fullLine);
            Assert.AreEqual("3 2 3 1 1 1", output);


        }

        [TestMethod]
        public void Parse_Full_Line_1_8__1()
        {
            var fullLine = TestUtility.CreateFullLineForTest_1_8__1();
            var parser = new Parser();
            string output = parser.ParseLine(fullLine);
            Assert.AreEqual("1 8 1", output);


        }

        [TestMethod]
        public void Test_Read_Grouped_logical_Lines()
        {
            
            List<string> lines = new List<string>() {
                "1234567890",
                "abcdefg",
                "hijklmn",
                ",.,.,.,.,.",
                "+++++++++",
                "---------------"
            };

            var lineReader = new LineReader(lines.ToArray());
            var linesGroups = lineReader.GroupedLines;

            Assert.AreEqual(2, linesGroups.Count);


        }

        
    }
}
